---
layout: page
title: Resources
title-zh: 资料
permalink: /resources/
css: pages
---
<ul class="l-links c-links">
  {% for resource in site.data.resources %}
  <li>
    <a class="l-link c-link c-link__{{ resource.category }}" href="{{ resource.link }}">
      <small class="l-link__tag">{{ resource.category }}</small>
      <p>{{ resource.title }}</p>
    </a>
  </li>
  {% endfor %}
</ul>
