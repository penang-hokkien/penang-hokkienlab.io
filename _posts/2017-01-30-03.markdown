---
layout: post
title:  "Han-na, han-naaa"
date:   2017-01-30 12:12:21 +0800
audio: 03.mp3
---
<div class="l-lang__en c-lang__en">
    <p>It's the second day of Chinese New Year, and I'm still lazing around in my old home back in JB (Johor Bahru, for all you non-Malaysians). And I caught myself using the phrase “han-na” in reply to something my mother was saying. I'm not too sure if this is standard Penang Hokkien usage or just us, but “han-na” is used as a response when you are obliged to do something.</p>
    <p>Specifically, it will come out when you are reminded to do something in the future. For example, if your mother says something along the lines of: remember to ask your sister to print the itinerary, you can reply with “han-na” as an indication that, yes, you will ask your sister to do that.</p>
    <p>You could always use the swiss army knife of replies: “orh”, but “orh” can sometimes be too general. The use of “han-na” implies that you've not only heard the request but are also obliged to carry out said request.</p>
    <p>The intonation of “han-na” is also important. A neutral sounding “han-na” is appropriate for all circumstances, where the “na” is subdued. But if the exact same request is repeated multiple times, you can use an exaggerated “han-na”, by dragging out the last syllable, to express your exasperation, becoming “han-naaa”.</p>
    <p>“Han-na” can be used in another context to mean “yes, of course”. It is usually invoked when you someone asks you a painfully obvious question. For example, if someone asks you, “You mean there are 7 colours in the rainbow?”, you can reply ”Han-na!” with the tone of “na” going up. If this description is as clear as mud, maybe listen to the audio <span class="o-emoji" role="img" tabindex="0" aria-label="backhand index pointing down">👇</span>.</p>̦
</div>

<div class="l-lang__zh c-lang__zh" lang="zh">
    <div class="ms-wrapper">
        <p>今天年初二，而我还赖在新山的老家。无意间注意到自己用了「喊拿」这个词回答母亲的某个问题。我也不清楚这是正统槟城福建话或则是我们家的用词，但是「喊拿」的用意是你愿意履行要求。</p>
        <p>要说详细一点就是当你被提醒做某件事时，你可以用「喊拿」表明你会去做。例如，妈妈要是说：记得叫姐姐把行程印出来哦，你可以回答「喊拿」，来表示你会去提醒姐姐。</p>
        <p>当然，你可以选择用最常用的「哦」来回答，但是有时「哦」的定义并不明确，你「哦」可能只是个敷衍的答复，但是用「喊拿」表示你真的会去做。</p>
        <p>「喊拿」的音节也非常重要。「拿」平时是用第三声来念，但是如果同样的要求再而三的重复，你可以把「拿」拖长来念，表示你已经有些不耐烦。</p>
        <p>最后，「喊拿」也有另外一个用法，就是「那当然」的意思。通常用在回答一些非常明显的问题。例如，有人问你，彩虹真的有七个颜色吗？你可以回答「喊呐」，「呐」用第四声来念。如果读不明我在说啥，不妨听听录音吧<span class="o-emoji" role="img" tabindex="0" aria-label="向下指">👇</span>。</p>

    </div>
</div>
