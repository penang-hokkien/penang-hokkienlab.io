---
layout: post
title:  "Not yet k-ouk"
date:   2016-12-26 12:12:21 +0800
audio: 02.mp3
---
<div class="l-lang__en c-lang__en">
    <p>As part of my Penang Hokkien clickfest, I also came across an old blog post by <a href="https://www.facebook.com/dcfc78?ref=profile">David Chan</a> about <a href="http://angkukuehblog.blogspot.sg/2007/02/part-ii.html">how we Northerners speak Hokkien</a>. It's highly entertaining, especially if you speak Penang Hokkien. And it also made me think about the various words only found in Penang Hokkien.</p>
    <p>The words I cover here may or may not be part of the actual Penang Hokkien vernacular, some of them could simply be unique to my family. That's why these posts are called stories, not facts. <span class="emoji" role="img" tabindex="0" aria-label="person shrugging">🤷‍</span></p>
    <p>Generally, Penang Hokkien includes a bunch of remixed Malay and English words, which are then pronounced in our unique accent such that you might even miss the fact that they are borrowed words.</p>
    <p>David's post covered the word “k-ouk” very well. In formal terms, “k-ouk” is an adverbial connector, like “lah”. But you cannot simply stick a “k-ouk” at the end of any phrase or sentence. If you use it wrongly, it will just end up being a joke.</p>
    <p>“K-ouk” is most commonly used with “not yet”. For example, if someone asks have you eaten, you can answer “not yet k-ouk” (probably best to just listen to the audio version of this <span class="o-emoji" role="img" tabindex="0" aria-label="backhand index pointing down">👇</span>). It sort of means “yet” when used this way. Like, has not come “k-ouk”, haven't finished “k-ouk”, you get the picture.</p>
    <p>But you can also use “k-ouk” to mean “still have”, as in I still have something left. If someone asks you whether there are any biscuits left in the tin, you can say “have k-ouk” (again, audio version makes so much more sense <span class="o-emoji" role="img" tabindex="0" aria-label="backhand index pointing down">👇</span>).</p>
    <p>Finally, “k-ouk” can also change the meaning of an answer from definitive, to temporary. Let me explain. If you ask me, do you want this piece of cake, and I say “don't want k-ouk”, it's a different meaning from an outright “don't want”. The addition of “k-ouk” implies I don't want it yet, but I probably want it later. So don't eat the cake. <span class="o-emoji" role="img" tabindex="0" aria-label="shortcake">🍰</span></p>
</div>

<div class="l-lang__zh c-lang__zh" lang="zh">
    <div class="ms-wrapper">
        <p>当我在寻找各式各样关于槟城福建话的连接时，无意间找到了<a href="https://www.facebook.com/dcfc78?ref=profile">大卫</a>的博客。他描述了我们<a href="http://angkukuehblog.blogspot.sg/2007/02/part-ii.html">北马人的福建话 </a>。熟悉槟城福建话的人看了应该会觉得很有趣。我也开始思考槟城福建话里一些独特的词汇。</p>
        <p>老实说，可能有一些词汇存粹只有在我家中使用，而并非是正式的槟城福建话。所以才说这些文章算是是故事，而并非事实。<span class="o-emoji" role="img" tabindex="0" aria-label="耸肩">🤷‍</span></p>
        <p>基本上，槟城福建话有许多英语及马来语的借词。而这些借词已经远离了源语的发音了。没注意的话，你可能都不会察觉到它们是借词。</p>
        <p>大卫谈到了「个哦」这个衔接语（要念成一个字来发音，还是点击下面的音频听听福建版本比较好啦<span class="o-emoji" role="img" tabindex="0" aria-label="向下指">👇</span>）。它跟「啦」的用法是很相似的。但是绝对不能随便在每个句子后面加个「个哦」，用错了会变成笑话。</p>
        <p>「个哦」通常会接着「还没有」来用。人家问你吃饱了没，你可回答「还没有个哦」，表示你还没吃饱。也可以用在「还没到个哦」，「还没做完个哦」等等。</p>
        <p>「个哦」也可以是「还有」的意思。例如，有人问你饼干桶里还有饼干吗？你可以说「有个哦」。（去福建版本吧<span class="o-emoji" role="img" tabindex="0" aria-label="向下指">👇</span>）</p>
        <p>最后，「个哦」有本事改编句子的意思。打个比方，如果你问我要不要吃蛋糕，而我给你的答复是「不要个哦」，这跟回答「不要」迥然不同。加了「个哦」表示我暂时不要吃，但是等会儿才要。所以别把我的蛋糕吃掉哦。<span class="o-emoji" role="img" tabindex="0" aria-label="蛋糕">🍰</span></p>
    </div>
</div>
