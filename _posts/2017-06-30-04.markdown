---
layout: post
title:  "Slim waist no good?"
date:   2017-06-30 12:12:21 +0800
audio: 04.mp3
---
<div class="l-lang__en c-lang__en">
    <p>This site is not dead, I promise. It's just that life gets in the way of things. This entry was just hiding in a corner of my brain waiting to be posted. And is really quite appropriate because I would use it in the context of this situation. This magic phrase is “Ee-ao xiu”.</p>
    <p>Okay, the romanisation is probably very wrong, but that's how it's said in my household. It is usually used as an expression of incredulous-ness, but not in a positive sense. Let's use it in a sentence.</p>
    <p>“Eh, do you know now cannot bring laptop into US on international flights?”</p>
    <p>“Ee-ao xiu, like that also can ah?”</p>
    <p>According to my dad, it actually literally translates to ‘Slim waist’ in Chinese, which implies that one will have a short life? So you wouldn't use it for something positive. At least, I've never heard it in that context. I honestly don't know how to verify this, maybe the next time I go back to Penang I will ask more people about the use of this phrase.</p>
    <p>Again, this might make more sense if you listen to the audio. Hopefully. <span class="o-emoji" role="img" tabindex="0" aria-label="backhand index pointing down">👇</span></p>
</div>

<div class="l-lang__zh c-lang__zh" lang="zh">
    <div class="ms-wrapper">
        <p>这个网站还没倒闭，真的。只是最近发生很多事，所以耽误了网站的更新。其实这份文章很早就想好了。所以这次的词语还蛮恰当的。它就是「腰咻」。</p>
        <p>总觉得这样的写法是错别字，但是我也不清楚这个词汇到底存不存在，只是我家是这样用的。它算是个感叹词，用在出乎预料的不良状况。打个比方：</p>
        <p>「你知道吗，现在美国将在国际航班禁带笔记本电脑耶！」</p>
        <p>「腰咻，这样都行啊？」</p>
        <p>我老爸说「腰咻」其实是「腰瘦」，指短命的意思。所以只在不良的状况用得着。我本身是没听过它用在好事身上。下次回槟城时我会再问问一些亲戚朋友，探个究竟吧。</p>
        <p>如果以上解释看不明白，不妨听听录音吧<span class="o-emoji" role="img" tabindex="0" aria-label="向下指">👇</span>。</p>
    </div>
</div>
